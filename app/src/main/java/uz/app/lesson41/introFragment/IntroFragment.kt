package uz.app.lesson41.introFragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import uz.app.lesson41.BotResponse
import uz.app.lesson41.BotResponse.Companion.GOOGLE
import uz.app.lesson41.BotResponse.Companion.SEARCH
import uz.app.lesson41.Constans.Companion.RECEIVE_ID
import uz.app.lesson41.Constans.Companion.SEND_ID
import uz.app.lesson41.R
import uz.app.lesson41.Time
import uz.app.lesson41.adapter.MessagingAdapter
import uz.app.lesson41.databinding.FragmentIntroBinding
import uz.app.lesson41.model.Message

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    private val messageAdapter = MessagingAdapter()
    private val botList = mutableListOf(
        "Ravshan",
        "Adam",
        "Selena",
        "Shoxruh"
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val random  = (botList.indices).random()
        sendBotMessage("Hello I'm  ${botList[random]}, How can I help you ? ")
        with(binding) {
            recycler.layoutManager = LinearLayoutManager(requireContext())
            recycler.adapter = messageAdapter
            btnSend.apply {
                setOnClickListener {
                    sendMessage()
                }
            }
        }

    }

    private fun sendMessage() {
        with(binding) {
            if (etText.text.isNotEmpty()) {
                val newMessage = Message(etText.text.toString(), SEND_ID, Time.timeStamp())
                messageAdapter.setElement(newMessage)
                botResponse(etText.text.toString())
                etText.text.clear()
            }
        }
    }
    private fun sendBotMessage(message:String){
        Handler().postDelayed({
                val newMessage = Message(message, RECEIVE_ID, Time.timeStamp())
                messageAdapter.setElement(newMessage)
        },800)

    }
    private fun botResponse(message: String) {
        Handler().postDelayed({

        val response = BotResponse.basicResponse(message)
            messageAdapter.setElement(Message(response, RECEIVE_ID, Time.timeStamp()))
        when(response){
            GOOGLE->{
                val intent =Intent(Intent.ACTION_VIEW)
              intent.data = Uri.parse("https://www.google.com")
                startActivity(intent)
            }
            SEARCH->{
                val query = message.split("search")
                val intent =Intent(Intent.ACTION_VIEW)
                val searchQuery  = query[1].trim()
                intent.data = Uri.parse("https://www.google.com/search?&q=$searchQuery")
                startActivity(intent)
            }

        }
        },500)

    }


}