package uz.app.lesson41

class BotResponse {
    companion object {
        fun basicResponse(_message: String): String {
            val random = (0..2).random()
            val message = _message.toLowerCase()
            return when {
                message.contains("hello") || message.contains("hi")  -> {
                    when (random) {
                        0 -> "Hello"
                        1 -> "What's up?"
                        2 -> "Hi my friend"
                        else -> "Error"
                    }
                }
                message.contains("how are you") -> {
                    when (random) {
                        0 -> "I am fine thank you..."
                        1 -> "I am good thanks..."
                        2 -> "Very well"
                        else -> "Error"
                    }
                }
                message.contains("time") && message.contains("?") -> {
                    Time.timeStamp()
                }
                message.contains("open") && message.contains("google") -> {
                   GOOGLE
                }
                message.contains("search") -> {
                    SEARCH
                }
                else -> {
                    when (random) {
                        0 -> "I don't understand"
                        1 -> "Please ask me something differnt"
                        2 -> "Sorry..."
                        else -> "Error"
                    }
                }
            }

        }
        val GOOGLE = "Opening the google"
        val SEARCH = "search"
    }
}