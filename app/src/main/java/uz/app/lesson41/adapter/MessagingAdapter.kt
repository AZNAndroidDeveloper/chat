package uz.app.lesson41.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.app.lesson41.Constans.Companion.RECEIVE_ID
import uz.app.lesson41.Constans.Companion.SEND_ID
import uz.app.lesson41.model.Message
import uz.app.lesson41.databinding.MessageItemBinding as  MessagingBind

class MessagingAdapter ():RecyclerView.Adapter<MessagingAdapter.MessagingViewHolder>(){

    val elements = mutableListOf<Message>()
    inner class MessagingViewHolder( binding:MessagingBind):RecyclerView.ViewHolder(binding.root){
        val sendMessage  = binding.tvMessage
        val botSendMessage = binding.tvMessageBox

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagingViewHolder {
        return MessagingViewHolder(MessagingBind.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int  = elements.size

    override fun onBindViewHolder(holder: MessagingViewHolder, position: Int) {
        val currentMessage = elements[position]
        when(currentMessage.id){
            SEND_ID->{
                holder.sendMessage.text = currentMessage.message
                holder.sendMessage.visibility = View.VISIBLE
                holder.botSendMessage.visibility = View.GONE
            }
            RECEIVE_ID->{
                holder.botSendMessage.text = currentMessage.message
                holder.botSendMessage.visibility = View.VISIBLE
                holder.sendMessage.visibility = View.GONE

            }
        }
    }

    fun setElement(element:Message){
        elements.apply {  add(element) }
        notifyDataSetChanged()
    }
}