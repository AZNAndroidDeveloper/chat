package uz.app.lesson41

import android.annotation.SuppressLint
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

class Time {
    companion object {
        @SuppressLint("SimpleDateFormat")
        fun timeStamp(): String =
            SimpleDateFormat("HH:mm").format(Date(Timestamp(System.currentTimeMillis()).time))

    }
}
